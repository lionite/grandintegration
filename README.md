# grandintegration

Automatizacion y documentacion de los procesos sysadmin para levantar la solucion CRM + WebRTC + PBX para clientes con grandi:

Que hace este playbook?:

1. Configuraciones en el Freepbx
  - Instala y configura glusterfs-server para levantar la carpeta de grabaciones como un volumen
  - Instala y configura asternic
  - Añade el dialplan custom para la integracion
  - Instala wombat dialer

2. Instalación y configuración de servidor webrtc
  - Instala la paquetería necesaria para buildear kamailio y rtpengine
  - Configura estas dos aplicaciones
  - Instala y configura nginx para funcionar como proxy de WSS y como web server para tener grabaciones disponibles en url
  - Instala y configura glusterfs-client para montar la carpeta de grabaciones de la pbx como un volumen en este server
