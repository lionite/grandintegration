#!/bin/bash

# Shell script para facilitar el deploy de webrtc server para grandi
#
# Autor: Andres Felipe Macias

PATH=$PATH:/root/.local
current_directory=`pwd`
export ANSIBLE_CONFIG=$current_directory
IS_ANSIBLE="`which ansible`"
arg1=$1
arg2=$2

UserValidation(){
  whoami="`whoami`"
  if [ "$whoami" == "root" ]; then
    echo "You have the permise to run the script, continue"
  else
    echo "You need to be root or have sudo permission to run this script, exiting"
    exit 1
  fi
}

Rama() {
    UserValidation
    echo "Creating the installation process log file /var/tmp/log/grandi_install"
    mkdir -p /var/tmp/log
    touch /var/tmp/log/grandi_install
}

Tag() {
  servers_ammount="`cat $current_directory/inventory | grep \"ansible_user=\"|wc -l`"
  for i in `seq 1 $servers_ammount`; do
    line="`cat $current_directory/inventory | grep \"ansible_ssh_port\" | sed -n ${i}p`"
    ip="`echo $line |awk -F \" \" '{print $4}' |awk -F "=" '{print $2}' `"
    ssh_port="`echo $line |grep ansible_ssh_port|awk -F " " '{print $2}'|awk -F "=" '{print $2}'`"
    echo "Transifiendo llave publica a servidores a instalar"
    ssh-copy-id -p $ssh_port -i /root/.ssh/id_rsa.pub root@$ip
  done
  ${IS_ANSIBLE}-playbook -s $current_directory/install.yml
  ResultadoAnsible=`echo $?`
  if [ $ResultadoAnsible == 0 ];then
    echo ""
  else
      echo ""
      echo "###################################################################################"
      echo "##             Installation failed, check what happened and try again            ##"
      echo "###################################################################################"
      echo ""
  fi
}
Rama
Tag
